## Это тестовый проект 
Он оформлен в виде библиотеки классов.

### Задание 1
Решение находится в классе CustomQuery, файл - [CustomQuery.cs](https://bitbucket.org/new_omega/kasperskylabtest/src/32196be0481afb8b494924571004315e0c24cbe5/KasperskyLabTest/CustomQuery.cs?at=master).

### Задание 2
Решение находится в классе SomeClass, файл - [SomeClass.cs](https://bitbucket.org/new_omega/kasperskylabtest/src/32196be0481afb8b494924571004315e0c24cbe5/KasperskyLabTest/SomeClass.cs).