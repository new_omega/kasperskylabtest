using System.Collections.Generic;
using System.Threading;

namespace KasperskyLabTest
{
    /*
     * Надо сделать очередь с операциями push(T) и T pop(). Операции должны поддерживать
     * обращение с разных потоков. Операция push всегда вставляет и выходит. Операция pop ждет пока
     * не появится новый элемент. В качестве контейнера внутри можно использовать только
     * стандартную очередь (Queue).
    */
    public sealed class CustomQueue<T>
    {
        private static object _lock = new object();
        private readonly AutoResetEvent waitHandle;
        private Queue<T> container;

        public CustomQueue()
        {
            container = new Queue<T>();
            waitHandle = new AutoResetEvent(false);
        }

        public void Push(T item)
        {
            lock (_lock)
            {
                container.Enqueue(item);
                waitHandle.Set();
            }
        }

        public T Pop()
        {
            waitHandle.WaitOne();

            lock (_lock)
            {
                return container.Dequeue();
            }
        }

        public void Close()
        {
            waitHandle.Close();
        }
    }
}