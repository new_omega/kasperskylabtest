using System;

namespace KasperskyLabTest
{
    public sealed class SomeClass
    {
        // Есть коллекция чисел и отдельное число Х. Надо вывести все пары чисел, которые в сумме
        // равны заданному Х.
        static void PrintAllPairs(int[] array, int x)
        {
            for (int i = 0; i < array.Length - 1; i++)
            {
                for (int j = i + 1; j < array.Length; j++)
                {
                    if (array[i] + array[j] == x)
                    {
                        Console.WriteLine("Пара {0} и {1} в сумме равна {2}", array[i], array[j], x);
                    }
                }
            }
        }
    }
}